# Git basics & Online git repository

> This is a simple **git** basics guide to get you started on your journey with **version control** or as a reminder

### Table of Contents

- [Install git](#1-install-git)
- [Make sure git is properly installed](#2-make-sure-git-is-properly-installed)
- [Set git configurations and review them](#3-set-basic-git-configurations-and-review-them)
- [Starting a new project](#4-starting-a-new-project)
- [Ignore tracking files in git](#5-ignore-tracking-files-in-git)
- [Reverting to a previous state](#6-reverting-to-a-previous-state)
- [Creating new branches](#7-creating-new-branches)
- [Creating a deployment branch](#8-creating-a-deployment-branch)
- [Setting Up an online git repository](#9-setting-up-an-online-git-repository)
- [Conclusion](#conclusion)
- [References](#references)

## 1. Install git

* Windows go to https://git-scm.com
* Mac go to https://git-scm.com
* Linux Debian based distribution:
	```shell
	$ sudo apt install git
	```

## 2. Make sure git is properly installed
* Type in command line
	```shell
	$ git --version
	```

## 3. Set basic git configurations and review them

* Set User Name
	```shell
	$ git config --global user.name "jon doe"
	```
* Set User Email
	```shell
	$ git config --global user.email 'jon.doe@gmail.com'
	```
* Review git configuration list
	```shell
	$ git config --list
	```

## 4. Starting a new project

When starting a new project do as follows
* Initialize the git repository
	```shell
	$ git init
	```
* Whenever you make changes to the project, you stage files with :
	```shell
	$ git add <NameOfTheFilesYouMadeChangesTo>
	```
	+ Or add all modified and new files with:
		```shell
		$ git add .
		```
* Whenever you wanna know the status of the changes in the project
	```shell
	$ git status
	```
* Commit the changes that you added to the staging area
	```shell
	$ git commit -m 'say something meaningful about what you are commiting( what you added or did to the project)'
	```
* See informations of the commits that have been made, who made them and when
	```shell
	$ git log
	```
	+ Less detailed log:
		```shell
		$ git log --oneline
		```

* Unstage a staged file without disturbing the working directory (leaves the changes but in an unstaged state and to stage them again use "git add")
	```shell
	$ git reset <FileName>
	```
	+ To Unstage all files
		```shell
		$ git reset
		```

## 5. Ignore tracking files in git

Ignore the tracking of certain files and directories in *git* is done by:
* Creating a .gitignore file
	```shell
	$ touch .gitignore
	```
* Add the _Name_ of file or directory you don't wanna track to that file

## 6. Reverting to a previous state

To Revert a file to a previous state in a previous commit
+ Find the desired commit number with the command **git log** and copy it to the clipboard and then:
	```shell
	$ git checkout <commitNumber> <FileName>
	```
+ Revert back to the recent commit of the file (if you made a mistake by reverting back to a previous commit)
	```shell
	$ git reset HEAD <fileName>
	$ git checkout -- <fileName>
	```

## 7. Creating new branches

When making changes and you don't wonna mess the master branch or making a contribution to an open source project

* Create a new branch based on the last commit on the current branch
	```shell
	$ git branch <NameOfBranch>
	```
	* Switch to the new branch
		```shell
		$ git checkout <NameOfBranch>
		```
* Or create the new branch and switch to it in one line
	```shell
	$ git checkout -b <NameOfBranch>
	```
* Create a new empty branch
	```shell
	$ git checkout --orphan <NameOfBranch>
	```
* Check the list of branches
	```shell
	$ git branch --list
	```
	* Or simply
		```shell
		$ git branch
		```

## 8. Creating a deployment branch

To create a deployment branch based a build folder (or whatever its name may be <dist>, <build> or <deploy>) in the master branch (Or whatever branch)
* First add & commit the folder
	```shell
	$ git add <folderName>
	$ git commit -m 'write a meaningful message for the commit'
	```
* Then: 
	```shell
	$ git subtree split --prefix <folderName> --branch <branchName>
	```
* Whenever changes are made to the folder and wanna update the deployment branch enter again
	```shell
	$ git add <folderName>
	$ git commit -m 'write a meaningful message for the commit'
	$ git subtree split --prefix <folderName> --branch <branchName>
	```
	--- Note : Don't modify the deployment branch manually

## 9. Setting Up an online git repository

* Create an account on Github.com
* Create an account on Bitbucket.com
* Create an account on Gitlab.com
* Or etc.

### 9.1. Create a new repository on one of them (github, bitbucket or gitlab)

* Copy the URL of the online git repository
* In the terminal in the location of the local repository
	```shell
	$ git remote add origin <UrlOnlineRepository>
	```
* Make sure the remote repository is properly added
	```shell
	$ git remote show origin
	```
* Create a _README.md_ (markdown language) where you describe the project. You can learn it in [here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet "click here") or [there](https://daringfireball.net/projects/markdown/syntax 'click here')
	```shell
	$ touch README.md
	```
* then:
	```shell
	$ git add README.md
	$ git commit -m 'Added readme.md'
	```
* Now time to push everything to your online git repository
	```shell
	$ git push -u origin <branchName>
	```
<branchName> is master because it's usually the branch you work on and push but you could specify a diffrent one depending on which branch you push

* Enter you credientials if you're prompted to (user name and password of your account)

### 9.2. Clone a project

Found a project on an online git repository that is interesting and you wanna have locally.
* Browse to a desired location on your computer and in command line
	```shell
	$ git clone <UrlOnlineRepository>
	```

## Conclusion

* Now normaly you are done with the basic git commands and ready to use git.

* Whenever you start to feel that you are confortable with all the above I urge you to check [How to contribute to an opensource project ?](https://codeburst.io/a-step-by-step-guide-to-making-your-first-github-contribution-5302260a2940 "click here")

## References

* https://git-scm.com
* https://git-scm.com/docs
* https://daringfireball.net/projects/markdown/syntax
* https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
